import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { numberPress, operatorPress, equalsPress, clearPress, setHasEqualsPressed, repeatEqualsPress, toggleOperator, percentagePress } from '../redux/actions/calcAction';
import CustomButton from './CustomButtons';
import LandscapeScreen from './LandscapeScreen';
import { landscapeStyles } from './LandscapeScreen';

export default function MainScreen() {
  const [activeButton, setActiveButton] = useState(null);
  const currentDisplayValue = useSelector((state) => state.calculator);
  const hasEqualsPressed = useSelector((state) => state.calculator.hasEqualsPressed);
  const dispatch = useDispatch();

  //Dimensions - объект, который содержит ширину и высоту экрана
  const [screenMode, setScreenMode] = useState(
    Dimensions.get('window').height > Dimensions.get('window').width ? 'portrait' : 'landscape'
  );

  useEffect(() => {
    let isMounted = true;//чтобы не вызыв setState, если компонент размонт-н 
    const handleOrientationChange = () => {
      if (isMounted) {
        const newScreenMode =
          Dimensions.get('window').height > Dimensions.get('window').width ? 'portrait' : 'landscape';
        setScreenMode(newScreenMode);
      }
    };
    Dimensions.addEventListener('change', handleOrientationChange);
    return () => {
      isMounted = false;
    };
  }, []);

  const containerMode = screenMode === 'portrait' ? portraitStyles : landscapeModeStyles;

  const arrayOfNumbersAndSymbol = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0, '.'];
  const arrayOfOperators = ['÷', '×', '-', '+'];

  const handlePress = (value) => {
    setActiveButton(value);//Подсветка нажатой кнопки
    if (value === 'C') {
      dispatch(numberPress('0'));
    } else if (value === 'AC') {
      dispatch(clearPress());
    } else if (!isNaN(parseFloat(value)) || value === '.') {
      if (hasEqualsPressed && currentDisplayValue.operator === null) {
        //Если нажато "=" и затем не нажат оператор, обнул рез-тат и начинаем новое вычисл
        dispatch(clearPress());
        dispatch(setHasEqualsPressed(false));
      }
      dispatch(numberPress(value));
    } else if (arrayOfOperators.includes(value)) {
      //Если нажимаем на оператор, то срабатывает equalsPress, кот устанавл рез-тат в previousValue и обнуляет displayValue
      if (currentDisplayValue.operator) {
        dispatch(equalsPress());
        dispatch(setHasEqualsPressed(true));
      }
      dispatch(operatorPress(value));
    } else if (value === '=') {
      dispatch(equalsPress());
      //Если повторно нажато "=" и затем не нажат оператор применяем к сумме предыдущего результата последний оператор и операнд
      if (hasEqualsPressed && currentDisplayValue.operator === null) {
        dispatch(repeatEqualsPress());
      }
      dispatch(setHasEqualsPressed(true));
    } else if (value === '+/-') {
      dispatch(toggleOperator());
    } else if (value === '%') {
      dispatch(percentagePress());
    }
  };

  const currentDisplayValueToString = currentDisplayValue.displayValue !== '0' ? parseFloat(currentDisplayValue.displayValue).toString() : parseFloat(currentDisplayValue.previousValue).toString();

  const portraitDisplayValueLimited=currentDisplayValueToString.slice(0, 9);
  const landscapeDisplayValueLimited=currentDisplayValueToString.slice(0,17);
  const fontSize = screenMode === 'portrait' ? (portraitDisplayValueLimited.length <= 7 ? 85 : portraitDisplayValueLimited.length === 8 ? 80 : portraitDisplayValueLimited.length === 9 ? 75 : 75) : 55;

  const displayMode = screenMode === 'portrait' ? portraitDisplayValueLimited : landscapeDisplayValueLimited;

  const toggleCleaningButton = (currentDisplayValue.displayValue !== '0' || currentDisplayValue.previousValue !== '0') ? 'C' : 'AC';

  return (
    <View style={containerMode.portraitWrapper}>
      <Text style={[containerMode.display, { fontSize }]}>{displayMode}</Text>
      <View style={containerMode.generalBtnsContainer}>
        {screenMode === 'landscape' && (
          <View style={containerMode.outerContainer}>
            <LandscapeScreen />
          </View>
        )}
        <View style={containerMode.buttonsContainer}>
          <View style={containerMode.leftBlock}>
            <View style={containerMode.upperBlock}>
              <CustomButton
                value={toggleCleaningButton}
                buttonStyle={containerMode.upperButton}
                onPress={() => handlePress('AC')}
                textStyle={containerMode.upperButtonText}
                underlayColor="white"
                setActiveButton={setActiveButton}
              />
              <CustomButton
                value={'+/-'}
                buttonStyle={containerMode.upperButton}
                onPress={() => handlePress('+/-')}
                textStyle={containerMode.upperButtonText}
                underlayColor="white"
                setActiveButton={setActiveButton}
              />
              <CustomButton
                value={'%'}
                buttonStyle={containerMode.upperButton}
                onPress={() => handlePress('%')}
                textStyle={containerMode.upperButtonText}
                underlayColor="white"
                setActiveButton={setActiveButton}
              />
            </View>
            <View style={containerMode.lowerBlock}>
              {arrayOfNumbersAndSymbol.map((number) => (
                <CustomButton
                  key={number}
                  value={number === '.' ? ',' : number.toString()}
                  buttonStyle={number === 0 ? containerMode.zeroLowerButton : containerMode.lowerButtons}
                  onPress={() => handlePress(number)}
                  textStyle={containerMode.lowerButtonText}
                  underlayColor="grey"
                  setActiveButton={setActiveButton}
                />
              ))}
            </View>
          </View>
          <View style={containerMode.asideBlock}>
            {arrayOfOperators.map((operator) => (
              <CustomButton
                key={operator}
                value={operator}
                buttonStyle={containerMode.asideButtons}
                onPress={() => handlePress(operator)}
                textStyle={[
                  containerMode.asideButtonsText,
                  { color: activeButton === operator ? 'darkorange' : 'white' },
                ]}
                underlayColor="white"
                setActiveButton={setActiveButton}
              />
            ))}
            <CustomButton
              value={'='}
              buttonStyle={containerMode.asideButton}
              onPress={() => handlePress('=')}
              textStyle={[
                containerMode.asideButtonText,
                { color: activeButton === '=' ? 'darkorange' : 'white' },
              ]}
              underlayColor="white"
              setActiveButton={setActiveButton}
            />
          </View>
        </View>
      </View>

    </View>
  );
}

const portraitStyles = StyleSheet.create({
  portraitWrapper: {
    flex: 1,
    width: '100%',
    gap: 15,
    padding: 15,
    paddingBottom: 30,
    justifyContent: 'flex-end',
  },
  display: {
    flex: 0,
    // fontSize: 85,
    textAlign: 'right',
    justifyContent: 'flex-end',
    color: 'white',
    height: 120,
  },
  buttonsContainer: {
    flexDirection: 'row',
    gap: 15,
    width: '100%',
  },
  leftBlock: {
    width: '75%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  upperBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  lowerBlock: {
    gap: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  asideBlock: {
    width: '25%',
    flexDirection: 'column',
    justifyContent: 'space-between',
    gap: 15,
  },
  upperButton: {
    width: 85,
    height: 85,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'darkgrey',
  },
  upperButtonText: {
    fontSize: 35,
    fontWeight: 'bold',
    color: 'black',
  },
  lowerButtons: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: '#333333',
    justifyContent: 'center',
    alignItems: 'center',
  },
  zeroLowerButton: {
    width: 185,
    height: 85,
    borderRadius: 50,
    backgroundColor: '#333333',
    justifyContent: 'center',
    alignItems: 'center'
  },
  lowerButtonText: {
    fontSize: 35,
    fontWeight: 'bold',
    color: 'white',
  },
  asideButtons: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: 'darkorange',
    justifyContent: 'center',
    alignItems: 'center',
  },
  asideButtonsText: {
    fontSize: 35,
    fontWeight: 'bold',
    // color: 'white',
  },
  asideButton: {
    width: 85,
    height: 85,
    borderRadius: 50,
    backgroundColor: 'darkorange',
    justifyContent: 'center',
    alignItems: 'center',
  },
  asideButtonText: {
    fontSize: 35,
    fontWeight: 'bold',
    color: 'white',
  },
});

const landscapeModeStyles = StyleSheet.create({
  ...landscapeStyles,
  ...portraitStyles,
  portraitWrapper: {
    ...portraitStyles.portraitWrapper,
    width: '100%',
    gap: 0,
    paddingRight: 0,
    paddingBottom: 0,
  },
  display: {
    ...portraitStyles.display,
    height: 90,
    paddingRight: 23,
  },
  leftBlock: {
    ...portraitStyles.leftBlock,
    width: '78%',
    paddingLeft: 15,
    paddingRight: 15,
  },
  upperBlock: {
    ...portraitStyles.upperBlock,
    gap: 20,
  },
  lowerBlock: {
    ...portraitStyles.lowerBlock,
    gap: 10,
  },
  asideBlock: {
    ...portraitStyles.asideBlock,
    gap: 0,
  },
  upperButton: {
    ...portraitStyles.upperButton,
    width: 65,
    height: 45,
  },
  lowerButtons: {
    ...portraitStyles.lowerButtons,
    width: 65,
    height: 45,
  },
  zeroLowerButton: {
    ...portraitStyles.zeroLowerButton,
    width: 150,
    height: 45,
  },
  asideButtons: {
    ...portraitStyles.asideButtons,
    width: 65,
    height: 45,
  },
  asideButton: {
    ...portraitStyles.asideButton,
    width: 65,
    height: 45,
  },
  upperButtonText: {
    ...portraitStyles.upperButtonText,
    fontSize: 20,
  },
  lowerButtonText: {
    ...portraitStyles.lowerButtonText,
    fontSize: 20,
  },
  asideButtonsText: {
    ...portraitStyles.asideButtonsText,
    fontSize: 20,
  },
  asideButtonText: {
    ...portraitStyles.asideButtonText,
    fontSize: 20,
  },
  buttonsContainer: {
    ...portraitStyles.buttonsContainer,
    width: '40%',
    height: '94%',
    gap: 5,
    paddingLeft: 5,
  },
  generalBtnsContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
  },
  outerContainer: {
    width: '60%',
  }
})
