import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import CustomButton from './CustomButtons';

export default function LandscapeScreen() {
  const [activeButton, setActiveButton] = useState(null);

  return (
    <View style={landscapeStyles.landscapeContainer}>
      <CustomButton
        value="("
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('(')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value=")"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress(')')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="mc"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('mc')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="m+"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('m+')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="m-"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('m-')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="mr"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('mr')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="2^nd"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('2^nd')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="x²"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('x^2')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="x³"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('x^3')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="xʸ"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('x^y')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="eˣ"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('e^x')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="10ˣ"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('10^x')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="1/x"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('1/x')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="²√x"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('sqrt(x)')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="∛x"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('cbrt(x)')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="ʸ√x"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('yrt(x)')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="ln"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('ln')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="log₁₀"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('log10')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="x!"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('x!')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="sin"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('sin')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="cos"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('cos')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="tan"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('tan')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="e"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('e')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="EE"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('EE')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="Rad"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('Rad')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="sinh"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('sinh')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="cosh"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('cosh')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="tanh"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('tanh')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="π"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('Pi')}
        //// onPress={() => handlePress('\u03C0')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
      <CustomButton
        value="Rand"
        buttonStyle={landscapeStyles.secondaryModeButtons}
        // onPress={() => handlePress('Rand')}
        textStyle={landscapeStyles.secondaryButtonsText}
        underlayColor="grey"
        setActiveButton={setActiveButton}
      />
    </View>
  )
}

export const landscapeStyles = StyleSheet.create({
  landscapeContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    gap: 10,
    backgroundColor: 'black',
    paddingLeft: 30,
  },
  secondaryModeButtons: {
    width: 65,
    height: 45,
    borderRadius: 50,
    backgroundColor: '#222222',
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryButtonsText: {
    color: 'white',
    fontSize: 16,
  },
});
