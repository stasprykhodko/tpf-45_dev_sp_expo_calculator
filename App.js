import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux/store';
// import { AppLoading } from 'expo';
import MainScreen from './src/components/MainScreen';
import { calculateResult } from './src/redux/reducers/calcReducer';

export default function App() {

  return (
    <Provider store={store}>
      <View style={styles.container}>
        <MainScreen />
        <StatusBar style="auto" />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100vw',
    height: '100vh',
    backgroundColor: 'black',
  },
});
