import calcReducer from './reducers/calcReducer';
import {legacy_createStore, combineReducers} from 'redux';

const rootReducer = combineReducers({
  calculator: calcReducer,
})
const store = legacy_createStore(rootReducer);

export default store;

