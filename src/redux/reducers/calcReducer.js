import { NUMBER_PRESS, OPERATOR_PRESS, EQUALS, REPEAT_EQUALS, HAS_EQUALS_PRESSED, TOGGLE_OPERATOR, PERCENTAGE_PRESS, CLEAR } from "../types"

const initialState = {
  displayValue: '0',
  previousValue: '0',
  operator: null,
  hasEqualsPressed: false,
  lastOperand: null,
  lastOperator: null,
}

const calcReducer = (state = initialState, action) => {

  switch (action.type) {
    case NUMBER_PRESS:
      return {
        ...state,
        displayValue:
          `${state.displayValue}${action.payload}`,
      };
    case OPERATOR_PRESS:
      return {
        ...state,
        previousValue: state.displayValue,
        displayValue: '0',
        operator: action.payload,

        lastOperator: action.payload
      };
    case EQUALS:
      const operand1 = state.previousValue !== null ? parseFloat(state.previousValue) : '0';
      const operand2 = parseFloat(state.displayValue);
      const operator = state.operator;

      switch (operator) {
        case '+':
          return {
            ...state,
            displayValue: `${operand1 + operand2}`,
            previousValue: null,
            operator: null,
            lastOperand: `${operand2}`,
          };
        case '-':
          return {
            ...state,
            displayValue: `${operand1 - operand2}`,
            previousValue: null,
            operator: null,
            lastOperand: `${operand2}`,
          };
        case '×':
          return {
            ...state,
            displayValue: `${operand1 * operand2}`,
            previousValue: null,
            operator: null,
            lastOperand: `${operand2}`,
          };
        case '÷':
          if (operand2 === '0') {
            return { ...state, displayValue: 'Error' };
          }
          return {
            ...state,
            displayValue: `${operand1 / operand2}`,
            previousValue: null,
            operator: null,
            lastOperand: `${operand2}`,
          };
        default:
          return state;
      }
    case HAS_EQUALS_PRESSED:
      return {
        ...state,
        hasEqualsPressed: action.payload,
      };
    case REPEAT_EQUALS:
      const operand3 = parseFloat(state.lastOperand);
      const operand4 = parseFloat(state.displayValue);
      const operator2 = state.lastOperator;

      switch (operator2) {
        case '+':
          return {
            ...state,
            displayValue: `${operand4 + operand3}`,
            previousValue: null,
            operator: null,
          };
        case '-':
          return {
            ...state,
            displayValue: `${operand4 - operand3}`,
            previousValue: null,
            operator: null,
          };
        case '×':
          return {
            ...state,
            displayValue: `${operand4 * operand3}`,
            previousValue: null,
            operator: null,
          };
        case  '÷':
          if (operand4 === '0') {
            return { ...state, displayValue: 'Error' };
          }
          return {
            ...state,
            displayValue: `${operand4 / operand3}`,
            previousValue: null,
            operator: null,
          };
        default:
          return state;
      }
    case TOGGLE_OPERATOR:
      return {
        ...state,
        displayValue: state.displayValue.startsWith('-')
          ? state.displayValue.slice(1)
          : `-${state.displayValue}`,
      };
    case PERCENTAGE_PRESS:
      const currentValue = state.displayValue;
      const previousValue = state.previousValue;
      let result;

      if (state.operator) {
        switch (state.operator) {
          case '+':
            result = previousValue * currentValue / 100;
            break;
          case '-':
            result = previousValue * currentValue / 100;
            break;
          case '×':
            result = currentValue / 100;
            break;
          case '÷':
            result = currentValue / 100;
            break;
          default:
            result = currentValue;
        }
      } else {
        result = currentValue / 100;
      }
      return {
        ...state,
        displayValue: result.toString(),
        // displayValue: `${parseFloat(state.displayValue) / 100}`,
      };
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export default calcReducer