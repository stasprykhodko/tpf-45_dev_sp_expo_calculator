import React from 'react';
import { TouchableHighlight, Text } from 'react-native';

const CustomButton = React.memo(function CustomButton(props) {
  const { setActiveButton, onPress, value, buttonStyle, textStyle, underlayColor } = props;
  const handleHideUnderlay = () => setActiveButton(null);
  const handleShowUnderlay = () => setActiveButton(value);

  return (
    <TouchableHighlight
      activeOpacity={0.6}
      underlayColor={underlayColor}
      style={buttonStyle}
      onPress={onPress}
      onHideUnderlay={handleHideUnderlay}
      onShowUnderlay={handleShowUnderlay}
    >
      <Text style={textStyle}>{value}</Text>
    </TouchableHighlight>
  )
}
);

export default CustomButton;

