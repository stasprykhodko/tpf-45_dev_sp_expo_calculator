import { NUMBER_PRESS, OPERATOR_PRESS, EQUALS, REPEAT_EQUALS, HAS_EQUALS_PRESSED, TOGGLE_OPERATOR, PERCENTAGE_PRESS, CLEAR } from "../types"

export const numberPress = (value) => ({
    type: NUMBER_PRESS,
    payload: value,
});

export const operatorPress = (operator) => ({
    type: OPERATOR_PRESS,
    payload: operator,
});

export const equalsPress = () => ({
    type: EQUALS,
});

export const clearPress = () => ({
    type: CLEAR,
});

export const setHasEqualsPressed = (value) => ({
    type: HAS_EQUALS_PRESSED,
    payload: value,
});

export const repeatEqualsPress = () => ({
    type: REPEAT_EQUALS,
});

export const toggleOperator = () => ({
    type: TOGGLE_OPERATOR,
})

export const percentagePress = () => ({
    type: PERCENTAGE_PRESS,
})